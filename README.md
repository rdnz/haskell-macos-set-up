# One simple Haskell set-up based on [ghcide](https://github.com/digital-asset/ghcide#readme) and [ghcid](https://github.com/ndmitchell/ghcid#readme) for Linux and macOS
## This content moved to https://gitlab.haskell.org/zander/haskell_set-up/-/wikis/contents.
